package clases;

public class Pastel {
String nombre;
String sabor;
int cachos ;
double precio;

public Pastel(String nombre, String sabor, int cachos, double precio) {
	super();
	this.nombre = nombre;
	this.sabor = sabor;
	this.cachos = cachos;
	this.precio = precio;
}

public Pastel() {
	super();
	this.nombre = "";
	this.sabor = "";
	this.cachos = 0;
	this.precio = 0;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getSabor() {
	return sabor;
}

public void setSabor(String sabor) {
	this.sabor = sabor;
}

public int getCachos() {
	return cachos;
}

public void setCachos(int cachos) {
	this.cachos = cachos;
}

public double getPrecio() {
	return precio;
}

public void setPrecio(double precio) {
	this.precio = precio;
}

@Override
public String toString() {
	return "Pasteles [nombre=" + nombre + ", sabor=" + sabor + ", cachos=" + cachos + ", precio=" + precio + "]";
}


		
	

}

package clases;

public class Tienda {

    Pastel[] pasteles;
    public Tienda(int maxpasteles) {
        this.pasteles = new Pastel[maxpasteles];
    }
//dar de alta pasteles
public void altaPastel(String nombre, String sabor, int cachos, double precio) {
for (int i = 0; i < pasteles.length; i++) {
    if (pasteles[i] == null) {
        pasteles[i]= new Pastel(nombre, sabor, cachos, precio);
        break;

    }
}
}

//metodo para que el programa haga un listado de los pasteles
public void listarpasteles() {
     for (int i=0;i<pasteles.length;i++) {
         if (pasteles[i]!=null) {
             System.out.println(pasteles[i]);
         }
     }

 }
//el metodo para buscar el pastel que quieras
public Pastel buscarpasteles(String nombre) {
 for (int i=0;i<pasteles.length;i++) {
     if (pasteles[i]!=null) {
         if (pasteles[i].getNombre().equals(nombre)) {
             return pasteles[i];
         }
     }
 }

 return null;
}

 //buscar cachos de un pastel
 public Pastel buscarcachospastel(String nombre) {
     for (int i=0;i<pasteles.length;i++) {
         if (pasteles[i]!=null) {
             if (pasteles[i].getNombre().equals(nombre)) {
                 return pasteles[i];
             }
         }
     }
     return null;
 }
 //metodo para eliminar un pastel
 public void eliminarPastel(String nombre) {
     for (int i=0;i<pasteles.length;i++) {
         if (pasteles[i]!=null) {
             if (pasteles[i].getNombre().equals(nombre)) {
                 pasteles[i]=null;
             }
         }
     }
 }
 //metodo para cambair el nombre del pastel 
 public void cambiarNombre(String nombre, String nombre2) {
     for (int i = 0; i < pasteles.length; i++) {
         if (pasteles[i].getNombre().equals(nombre)) {
             pasteles[i].setNombre(nombre2);
         }
     }
 }
//Listar todos los pasteles con un precio superior de 17�//
 public void listarPastelesPorPrecio(String precio) {
     for (int i = 0; i < pasteles.length; i++) {
         if (pasteles[i] !=null) {
             if (pasteles[i].getPrecio()>17.00) {
                 System.out.println(pasteles[i]);

             }
         }
     }
 }
 //metodo para listar por sabor
 public void listarPastelesPorSabor(String sabor) {
		for (int i=0;i<pasteles.length;i++) {
			if (pasteles[i]!=null) {
				if (pasteles[i].getSabor().equals(sabor)) {
					System.out.println(pasteles[i]);
             }
         }
     }
 }
}



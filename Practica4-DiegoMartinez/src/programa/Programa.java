package programa;
public class Programa {
		public static void main(String args[]) {

		        System.out.println("Creo La tienda");
		        int maxTiendas=4;
		        clases.Tienda miTienda = new clases.Tienda(maxTiendas);
		        
		        //Metodo para dar de alta a 4 Tartas//
		        System.out.println("Doy de alta 4 Tartas/Pasteles ");
		        miTienda.altaPastel("tarta de fresa", "Fresa", 10, 20.00);
		        miTienda.altaPastel("tarta de chocolate", "chocolate", 7, 10.00);
		        miTienda.altaPastel("tarta de limon", "limon", 20, 29.00);
		        miTienda.altaPastel("tarta de vainilla", "vainilla", 16, 24.00);
		        

		        //Metodo para listar a todas las tartas//
		        System.out.println("3.- Listar medicos");
		        miTienda.listarpasteles();

		        //Metodo para buscar el pastel segun el nombre//
		        System.out.println("Buscar un medico por su nombre");
		        System.out.println(miTienda.buscarpasteles("tarta de fresa"));

		        //Metodo para eliminar el pastel seleccionado por le nombre//
		        System.out.println("Eliminar un Pastel");
		        miTienda.eliminarPastel("Dtarta de fresa");
		        miTienda.listarpasteles();

		        //Metodo para almacenar un nuevo pastel creado//
		        System.out.println("Almacenamos un nuevo Pastel");
		        miTienda.altaPastel("Tarta de queso", "Queso", 18, 36.00);
		        miTienda.listarpasteles();

		        
		        System.out.println("Cambiar nombre pastel");
		        miTienda.cambiarNombre("Tarta de chocolate", "Le tartaa de la  chocolataaa");
		        miTienda.listarpasteles();

		        System.out.println("Listar Pasteles por  Sabor");
		        miTienda.listarPastelesPorSabor("fresa");

		        System.out.println("Listar Pasteles por  Precio");
		        miTienda.listarPastelesPorPrecio("15");
		        
		    }

		}
